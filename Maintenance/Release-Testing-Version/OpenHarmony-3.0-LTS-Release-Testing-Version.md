# OpenHarmony-V3.0.8-LTS

## OpenHarmony_3.0.8.2版本转测试信息

| 转测试版本号：OpenHarmony_3.0.8.2                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.0.8.2版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2023/02/14                             |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.2/20230214_011747/version-Release_Version-OpenHarmony-3.0.8.2-20230214_011747-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 Liteos: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.2/20230214_011757/version-Release_Version-OpenHarmony-3.0.8.2-20230214_011757-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.2/20230214_001910/version-Release_Version-OpenHarmony-3.0.8.2-20230214_001910-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2023/02/14                               |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.2/20230214_001924/version-Release_Version-OpenHarmony-3.0.8.2-20230214_001924-hispark_taurus_L2_3_0-LTS_daily.tar.gz |

## OpenHarmony_3.0.8.1版本转测试信息

| 转测试版本号：OpenHarmony_3.0.8.1                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.0.8.1版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2023/02/07                             |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.1/20230207_011755/version-Release_Version-OpenHarmony-3.0.8.1-20230207_011755-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 Liteos: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.1/20230207_011747/version-Release_Version-OpenHarmony-3.0.8.1-20230207_011747-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.1/20230207_001808/version-Release_Version-OpenHarmony-3.0.8.1-20230207_001808-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2023/02/07                               |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.8.1/20230207_001847/version-Release_Version-OpenHarmony-3.0.8.1-20230207_001847-hispark_taurus_L2_3_0-LTS_daily.tar.gz |

# OpenHarmony-V3.0.7-LTS

## OpenHarmony_3.0.7.2版本转测试信息

| 转测试版本号：OpenHarmony_3.0.7.2                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.0.7.2版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/11/21                             |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.2/20221121_011744/version-Release_Version-OpenHarmony-3.0.7.2-20221121_011744-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 Liteos: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.2/20221121_011625/version-Release_Version-OpenHarmony-3.0.7.2-20221121_011625-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.2/20221121_001633/version-Release_Version-OpenHarmony-3.0.7.2-20221121_001633-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2022/11/21                               |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.2/20221121_001636/version-Release_Version-OpenHarmony-3.0.7.2-20221121_001636-hispark_taurus_L2_3_0-LTS_daily.tar.gz |

## OpenHarmony_3.0.7.1版本转测试信息

| 转测试版本号：OpenHarmony_3.0.7.1                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.0.7.1版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/11/07                             |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.1/20221107_011623/version-Release_Version-OpenHarmony-3.0.7.1-20221107_011623-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 Liteos: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.1/20221107_011747/version-Release_Version-OpenHarmony-3.0.7.1-20221107_011747-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.1/20221107_001752/version-Release_Version-OpenHarmony-3.0.7.1-20221107_001752-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2022/11/07                               |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.7.1/20221107_001639/version-Release_Version-OpenHarmony-3.0.7.1-20221107_001639-hispark_taurus_L2_3_0-LTS_daily.tar.gz |

# OpenHarmony-V3.0.6-LTS

## OpenHarmony_3.0.6.2版本转测试信息

| 转测试版本号：OpenHarmony_3.0.6.2                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.0.6.2版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/08/25                             |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.2/20220825_130146/version-Release_Version-OpenHarmony-3.0.6.2-20220825_130146-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 Liteos: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.2/20220825_131624/version-Release_Version-OpenHarmony-3.0.6.2-20220825_131624-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.2/20220825_131631/version-Release_Version-OpenHarmony-3.0.6.2-20220825_131631-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2022/08/25                               |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.2/20220825_131625/version-Release_Version-OpenHarmony-3.0.6.2-20220825_131625-hispark_taurus_L2_3_0-LTS_daily.tar.gz |

## OpenHarmony_3.0.6.1版本转测试信息

| 转测试版本号：OpenHarmony_3.0.6.1                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.0.6.1版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/08/17                             |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.1/20220817_095155/version-Release_Version-OpenHarmony-3.0.6.1-20220817_095155-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 Liteos: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.1/20220817_095207/version-Release_Version-OpenHarmony-3.0.6.1-20220817_095207-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.1/20220817_095114/version-Release_Version-OpenHarmony-3.0.6.1-20220817_095114-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2022/08/17                               |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.0.6.1/20220817_102132/version-Release_Version-OpenHarmony-3.0.6.1-20220817_102132-hispark_taurus_L2_3_0-LTS_daily.tar.gz |



## OpenHarmony_3.0.5.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.1.5.2 *****          |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.5.2版本:             |
| **API****变更：**：                                          |
| **L0L1****转测试时间：2022/5/16                              |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.2/20220617_010153/version-Release_Version-OpenHarmony 3.0.5.2-20220617_010153-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 LiteOS: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.2/20220617_010145/version-Release_Version-OpenHarmony 3.0.5.2-20220617_010145-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.2/20220617_000143/version-Release_Version-OpenHarmony 3.0.5.2-20220617_000143-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2022/5/16                                |
| **L2****转测试版本获取路径：                                 |
| L2-hi3516版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.2/20220617_000029/version-Release_Version-OpenHarmony 3.0.5.2-20220617_000029-hispark_taurus_L2_3_0-LTS_daily.tar.gz |

## OpenHarmony_3.0.5.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.1.5.1 *****          |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.5.1版本:             |
| **API****变更：**：                                          |
| **L0L1****转测试时间：2022/5/9                               |
| **L0L1****转测试版本获取路径：                               |
| L0:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.1/20220609_094154/version-Release_Version-OpenHarmony 3.0.5.1-20220609_094154-hispark_pegasus_3_0-LTS_daily.tar.gz |
| L1 LiteOS:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.1/20220609_094028/version-Release_Version-OpenHarmony 3.0.5.1-20220609_094028-hispark_taurus_LiteOS_3_0-LTS_daily.tar.gz |
| L1 Linux:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.1/20220609_094141/version-Release_Version-OpenHarmony 3.0.5.1-20220609_094141-hispark_taurus_Linux_3_0-LTS_daily.tar.gz |
| **L2****转测试时间：2022/5/9                                 |
| **L2****转测试版本获取路径：                                 |
| L2-hi3516版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.0.5.1/20220609_101516/version-Release_Version-OpenHarmony 3.0.5.1-20220609_101516-hispark_taurus_L2_3_0-LTS_daily.tar.gz |

## 



# OpenHarmony 3.0.0.24版本转测试信息

转测试版本号：OpenHarmony 3.0.0.24
版本用途：tag标签版本，V3.0.3
转测试时间：2022/3/22
版本获取路径：
OpenHarmony_3.0_LTS_3.0.0.24：
hispark_pegasus_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.24/20220322_010011/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.24-20220322_010011-hispark_pegasus_3_0-LTS.tar.gz

hispark_taurus_Linux_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.24/20220322_000116/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.24-20220322_000116-hispark_taurus_Linux_3_0-LTS.tar.gz

hispark_taurus_LiteOS_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.24/20220322_010125/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.24-20220322_010125-hispark_taurus_LiteOS_3_0-LTS.tar.gz

hispark_taurus_L2_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.24/20220322_000024/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.24-20220322_000024-hispark_taurus_L2_3_0-LTS.tar.gz



# OpenHarmony 3.0.0.23版本转测试信息

转测试版本号：OpenHarmony 3.0.0.23
版本用途：tag标签版本，V3.0.2
转测试时间：2022/2/28
版本获取路径：
OpenHarmony_3.0_LTS_3.0.0.23：
hispark_pegasus_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.23/20220228_150123/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.23-20220228_150123-hispark_pegasus_3_0-LTS.tar.gz

hispark_taurus_LiteOS_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.23/20220228_150214/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.23-20220228_150214-hispark_taurus_LiteOS_3_0-LTS.tar.gz

hispark_taurus_Linux_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.23/20220228_150035/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.23-20220228_150035-hispark_taurus_Linux_3_0-LTS.tar.gz

hispark_taurus_L2_3_0-LTS：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.23/20220228_150439/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.23-20220228_150439-hispark_taurus_L2_3_0-LTS.tar.gz



# OpenHarmony 3.0.0.22版本转测试信息

转测试版本号：OpenHarmony 3.0.0.22
版本用途：tag标签版本，v3.0.1
转测试时间：2022/2/14
版本获取路径：

hispark_pegasus_3_0-LTS：

http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.22/20220214_160317/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.22-20220214_160317-hispark_pegasus_3_0-LTS.tar.gz

hispark_taurus_LiteOS_3_0-LTS：

http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.22/20220214_162518/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.22-20220214_162518-hispark_taurus_LiteOS_3_0-LTS.tar.gz

hispark_taurus_Linux_3_0-LTS：

http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.22/20220214_161247/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.22-20220214_161247-hispark_taurus_Linux_3_0-LTS.tar.gz

hispark_taurus_L2_3_0-LTS：

http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.22/20220214_161711/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.22-20220214_161711-hispark_taurus_L2_3_0-LTS.tar.gz

# OpenHarmony 3.0.0.21版本转测试信息

转测试版本号：OpenHarmony 3.0.0.21
版本用途：tag标签版本
转测试时间：2021/12/29
版本获取路径：
hispark_pegasus_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_010124/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_010124-hispark_pegasus_3_0-LTS.tar.gz

hispark_taurus_LiteOS_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_030136/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_030136-hispark_taurus_LiteOS_3_0-LTS.tar.gz

hispark_aries_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_030126/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_030126-hispark_aries_3_0-LTS.tar.gz

hispark_taurus_L2_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_000048/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_000048-hispark_taurus_L2_3_0-LTS.tar.gz

hispark_taurus_Linux_3_0-LTS
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.21/20211229_000126/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.21-20211229_000126-hispark_taurus_Linux_3_0-LTS.tar.gz



# OpenHarmony 3.0.0.20版本转测试信息

转测试版本号：OpenHarmony 3.0.0.20
版本用途：内部测试
转测试时间：2021/12/17
版本获取路径：

hispark_taurus_L2_3_0-LTS（L2）
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_000122/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_000122-hispark_taurus_L2_3_0-LTS.tar.gz
hispark_pegasus_3_0-LTS（L0）：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_080128/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_080128-hispark_pegasus_3_0-LTS.tar.gz
hispark_aries_3_0-LTS（L1 3518）
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_030127/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_030127-hispark_aries_3_0-LTS.tar.gz
hispark_taurus_LiteOS_3_0-LTS（L1 3516 LiteOS） 
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_030128/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_030128-hispark_taurus_LiteOS_3_0-LTS.tar.gz
hispark_taurus_Linux_3_0-LTS (L1 3516 Linux)
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.0_LTS_3.0.0.20/20211217_000134/version-Release_Version-OpenHarmony_3.0_LTS_3.0.0.20-20211217_000134-hispark_taurus_Linux_3_0-LTS.tar.gz

L2_SDK_Mac_LTS：（L2 SDK包）
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_3.0_LTS/20211217_004915/L2-SDK-MAC.tar.gz